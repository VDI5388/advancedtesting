package com.sda.advancedTesting.zoo.repository;

import com.sda.advancedTesting.zoo.entity.Animal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository

public interface AnimalRepository extends JpaRepository <Animal, Integer> {
}
