package com.sda.advancedTesting.zoo.service;
import com.sda.advancedTesting.zoo.entity.Animal;
import com.sda.advancedTesting.zoo.repository.AnimalRepository;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Service;
@Service
public class AnimalService {
    private AnimalRepository animalRepository;
    public AnimalService(AnimalRepository animalRepository) {
        this.animalRepository = animalRepository;
    }
    // animalToBeSaved (id = null, race = Lion, name = Tobby)
    // savedAnimal (id = 12321, race = Lion, name = Tobby)
    public Animal saveAnimal(Animal animalToBeSaved) {
//        if(animalToBeSaved.getRace()!= null && animalToBeSaved.getRace() != Strings.EMPTY
//                && animalToBeSaved.getRace() != " "){
//
//        }
        if (Strings.isBlank(animalToBeSaved.getRace())) {
            throw new IllegalArgumentException("Race is a mandatory field!");
        }
        Animal savedAnimal = animalRepository.save(animalToBeSaved);
        return savedAnimal;
    }
}