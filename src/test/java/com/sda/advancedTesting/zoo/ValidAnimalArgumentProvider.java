package com.sda.advancedTesting.zoo;


import com.sda.advancedTesting.zoo.entity.Animal;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;

import java.util.stream.Stream;

public class ValidAnimalArgumentProvider implements ArgumentsProvider {
    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) throws Exception {
        return Stream.of(
                        Arguments.of(new Animal(null, "Lion", "Tobby"),new Animal(3, "Lion", "Tobby")),
                        Arguments.of(new Animal(3, "Lion", "Tobby"),new Animal(3, "Lion", "Tobby")),
                        Arguments.of(new Animal(3, "Lion", null),new Animal(3, "Lion", null)),
                        Arguments.of(new Animal(null, "Lion", null),new Animal(3, "Lion", null)))
        ;
    }
}
