package com.sda.advancedTesting.zoo;
import com.sda.advancedTesting.zoo.entity.Animal;
import com.sda.advancedTesting.zoo.repository.AnimalRepository;
import com.sda.advancedTesting.zoo.service.AnimalService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;
import static org.assertj.core.api.Assertions.*;
@ExtendWith(MockitoExtension.class)
public class AnimalServiceTest {
    @Mock
    private AnimalRepository mockedAnimalRepo;

    @BeforeAll
    static void beforeAll(){
        System.out.println("Testing animal service!");
    };

    @BeforeEach
    void beforeEach(){

        System.out.println("Start a new test" + LocalDateTime.now());
    }

    @Test
    void verifySavedAnimal(){
        AnimalService animalService = new AnimalService(mockedAnimalRepo);
        Animal toBeSaved = new Animal(null, "T-Rex","Rex");
        Animal x = new Animal(1, "T-Rex","Rex");

        when(mockedAnimalRepo.save(toBeSaved)).thenReturn(x);
        assertThat(animalService.saveAnimal(toBeSaved)).isEqualTo(x);
    }

    @Test
    void errorOnSavingWithoutRace() {
        AnimalService service = new AnimalService(mockedAnimalRepo);
        Animal a1 = new Animal(null, "", "Toby");

        IllegalArgumentException raceIsMandatory =
        assertThrows(IllegalArgumentException.class, () -> {
            service.saveAnimal(a1);
        });
        assertEquals ("Race is a mandatory field!", raceIsMandatory.getMessage());
    }

    @ArgumentsSource(ValidAnimalArgumentProvider.class)
    @ParameterizedTest
    void verifySaveAnimalParameterized(Animal toBeSaved, Animal returnedByRepo) {
        AnimalService animalService = new AnimalService(mockedAnimalRepo);

        when(mockedAnimalRepo.save(toBeSaved)).thenReturn(returnedByRepo);

        assertThat(animalService.saveAnimal(toBeSaved)).isEqualTo(returnedByRepo);
    }

}