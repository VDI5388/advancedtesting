package com.sda.advancedTesting.calculator;

import com.sda.advancedTesting.calculator.Calculator;
import com.sda.advancedTesting.calculator.exceptions.TruncatedResultException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.AssertionsForClassTypes.assertThatExceptionOfType;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class CalculatorTest {
    @Test
    void verifyAdd() {
        //given
        double a = 20;
        double b = 10;
        Calculator calc = new Calculator();
        //when
        double result = calc.add(a, b);
        //then
        assertEquals(30, result);//primul parametru = rezultatul asteptat, al doilea parametru = resultatul returnat de metoda testata
    }

    @Test
    void verifySubtract() {
        //given
        double a = 20;
        double b = 10;
        Calculator calc = new Calculator();
        //when
        double result = calc.subtract(a, b);
        //then
        assertEquals(10, result);//primul parametru = rezultatul asteptat, al doilea parametru = resultatul returnat de metoda testata
    }

    @Test
    void verifyMultiply() {
        //given
        double a = 20;
        double b = 10;
        Calculator calc = new Calculator();
        //when
        double result = calc.multiply(a, b);
        //then
        assertEquals(200, result);//primul parametru = rezultatul asteptat, al doilea parametru = resultatul returnat de metoda testata
    }

    @Test
    void verifyDivide() {
        //given
        double a = 20;
        double b = 10;
        Calculator calc = new Calculator();
        //when
        double result = calc.divide(a, b);
        //then
        assertEquals(2, result);//primul parametru = rezultatul asteptat, al doilea parametru = resultatul returnat de metoda testata
    }

    @Test
    void verifyModulo() {
        //given
        int a = 20;
        int b = 10;
        Calculator calc = new Calculator();
        //when
        int result = calc.modulo(a, b);
        //then
        assertEquals(0, result);//primul parametru = rezultatul asteptat, al doilea parametru = resultatul returnat de metoda testata
    }

    @Test
    void multiplyThrowsTruncatedResultException() {

        //GIVEN
        double a = 10000;
        double b = Double.MAX_VALUE / (a - 1);

        Calculator c = new Calculator();

        //WHEN

        assertThatExceptionOfType(TruncatedResultException.class).isThrownBy(
                () -> c.multiply(a, b));
        {
        }

    }

    @Test
    void divideIllegalArgumentException() {

        //GIVEN
        double a = 10;
        double b = 0;

        Calculator c = new Calculator();

        //WHEN

        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(
                () -> c.divide(a, b)).withMessage("Division by zero is not allowed!");
        {
        }

    }
    // Tema de casa sa facem si exceptia pt modulo ca sa fie coverage 100% !!!

}
